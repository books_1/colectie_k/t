# T

## Content

```
./T. A. Barron:
T. A. Barron - Merlin - V1 Anii pierduti 1.0 '{Fantasy}.docx
T. A. Barron - Merlin - V2 Cele sapte canturi 1.0 '{Fantasy}.docx

./T. R. Richmond:
T. R. Richmond - Ce-a lasat in urma 0.9 '{Thriller}.docx

./T. S. Learner:
T. S. Learner - Sfinxul 0.9 '{Tineret}.docx

./T. Semuskin:
T. Semuskin - Alitet pleaca in munti 2.0 '{Literatura}.docx

./Tadeusz Dolega Mostowicz:
Tadeusz Dolega Mostowicz - V1 Vraciul 3.0 '{Thriller}.docx
Tadeusz Dolega Mostowicz - V2 Profesorul Wilczur 3.0 '{Thriller}.docx

./Tadeusz Konwicki:
Tadeusz Konwicki - Mica apocalipsa 0.8 '{Diverse}.docx

./Tahar Jelloun:
Tahar Jelloun - Noaptea sacra 1.0 '{Literatura}.docx

./Taichi Yamada:
Taichi Yamada - N-am mai visat de mult ca zbor 1.0 '{Literatura}.docx

./Takashi Hiraide:
Takashi Hiraide - Pisica musafir 1.0 '{Literatura}.docx

./Taku Mayumura:
Taku Mayumura - Invierea 1.0 '{SF}.docx
Taku Mayumura - Robotul atotstiutor 1.0 '{SF}.docx

./Tami Hoag:
Tami Hoag - Elena Estes - V1 Mastile crimei 1.0 '{Thriller}.docx
Tami Hoag - Inalt, brunet, indragostit 1.0 '{Romance}.docx
Tami Hoag - Iubire fermecata 1.0 '{Romance}.docx
Tami Hoag - Necazuri cu vecinul 0.99 '{Dragoste}.docx

./Tammara Webber:
Tammara Webber - E usor sa te iubesc 1.0 '{Dragoste}.docx

./Tamora Pierce:
Tamora Pierce - Cercul de Magie - V1 Magia lui Sandry 1.0 '{Tineret}.docx
Tamora Pierce - Cercul de Magie - V2 Magia lui Tris 1.0 '{Tineret}.docx
Tamora Pierce - Cercul de Magie - V3 Magia lui Daja 1.0 '{Tineret}.docx
Tamora Pierce - Cercul de Magie - V4 Magia lui Briar 1.0 '{Tineret}.docx

./Tana French:
Tana French - Linistea umbrelor 1.0 '{Literatura}.docx
Tana French - Padurea 1.0 '{Politista}.docx

./Tanti Mili:
Tanti Mili - Tanti Mili & the Black Angels 0.9 '{Umor}.docx
Tanti Mili - Tanti Mili si teroristii 0.9 '{Umor}.docx

./Tan Twan Eng:
Tan Twan Eng - Darul ploii 2.0 '{Literatura}.docx
Tan Twan Eng - Gradina ceturilor din amurg 1.0 '{Literatura}.docx

./Tanya Huff:
Tanya Huff - Cartile Sangelui - V1 Pretul sangelui 1.0 '{Vampiri}.docx

./Tanya Maguire:
Tanya Maguire - Atmosfera incordata 0.99 '{Romance}.docx

./Tara Janzen:
Tara Janzen - Pe fuga 1.0 '{ActiuneComando}.docx

./Tara Lee:
Tara Lee - Rasturnare de situatie 0.99 '{Dragoste}.docx

./Taran Matharu:
Taran Matharu - Invocatorul - V1 Novicele 1.0 '{Supranatural}.docx
Taran Matharu - Invocatorul - V2 Inchizitia 1.1 '{Supranatural}.docx

./Tariq Ali:
Tariq Ali - La umbra rodiului 0.99 '{AventuraIstorica}.docx

./Tarjei Vesaas:
Tarjei Vesaas - Palatul de gheata 1.0 '{Supranatural}.docx

./Tarun Tejpal:
Tarun Tejpal - Alchimia dorintei 0.99 '{Literatura}.docx
Tarun Tejpal - Povestea asasinilor mei 1.0 '{Literatura}.docx

./Tasha Alexander:
Tasha Alexander - Sezonul otravit 1.0 '{AventuraIstorica}.docx

./Tashira Tachi Ren:
Tashira Tachi Ren - Comunicare a arhanghelului Ariel 0.9 '{Spiritualitate}.docx

./Tate Mckenna:
Tate Mckenna - O mostenire din dragoste 0.9 '{Romance}.docx

./Tatiana de Rosnay:
Tatiana de Rosnay - Se numea Sarah 1.0 '{Literatura}.docx

./Tatiana Flondor Ariesanu:
Tatiana Flondor Ariesanu - Straina 0.99 '{Diverse}.docx

./Tatiana Niculescu:
Tatiana Niculescu - Mistica rugaciunii si a revolverului 1.0 '{Filozofie}.docx

./Tatiana Tolstaia:
Tatiana Tolstaia - Zatul 1.0 '{Literatura}.docx

./Tatiana Topciu:
Tatiana Topciu - File din povestea propriei mele vieti 0.9 '{Biografie}.docx

./Taylor Jenkins Reid:
Taylor Jenkins Reid - Fericiti pentru totdeauna 1.0 '{Dragoste}.docx
Taylor Jenkins Reid - Uniti pe vecie 1.0 '{Dragoste}.docx

./Taylor Stevens:
Taylor Stevens - Vanessa Munroe - V1 Femeia cameleon 2.0 '{Thriller}.docx
Taylor Stevens - Vanessa Munroe - V2 Inocentii 1.0 '{Thriller}.docx

./Ted Chiang:
Ted Chiang - Impartirea la zero 2.0 '{SF}.docx
Ted Chiang - Povestea vietii tale 0.99 '{SF}.docx
Ted Chiang - Turnul Babilonului 0.99 '{SF}.docx

./Ted Dekker:
Ted Dekker - Trilogia Cercului - V1 Negru. Matca raului 1.0 '{SF}.docx
Ted Dekker - Trilogia Cercului - V2 Rosu. Imposibila salvare 1.0 '{SF}.docx
Ted Dekker - Trilogia Cercului - V3 Alb. Fantastica urmarire 1.0 '{SF}.docx

./Tennessee Williams:
Tennessee Williams - Menajeria de sticla 0.9 '{Teatru}.docx
Tennessee Williams - Teatru 1.0 '{Teatru}.docx

./Teoclit Dionisiatul:
Teoclit Dionisiatul - Intre cer si pamant 1.0 '{Religie}.docx

./Teoctist:
Teoctist - Al cincilea patriarh 0.9 '{Religie}.docx

./Teodor Mazilu:
Teodor Mazilu - Cine pe cine mantuieste 1.0 '{Teatru}.docx
Teodor Mazilu - Don Juan moare ca toti ceilalti 1.0 '{Teatru}.docx
Teodor Mazilu - Frumos e in septembrie la Venetia 0.99 '{Teatru}.docx
Teodor Mazilu - O sarbatoare princiara 1.0 '{Dragoste}.docx
Teodor Mazilu - O singura noapte eterna 1.0 '{Dragoste}.docx
Teodor Mazilu - Palaria de pe noptiera 1.0 '{Teatru}.docx
Teodor Mazilu - Prostii sub clar de luna 0.99 '{Teatru}.docx
Teodor Mazilu - Treziti-va in fiecare dimineata 1.0 '{Teatru}.docx

./Teodor Parapiru:
Teodor Parapiru - Medalionul 2.0 '{Politista}.docx

./Teodor Popescu:
Teodor Popescu - Despre suferinta 0.9 '{Religie}.docx
Teodor Popescu - O inima curata 0.9 '{Religie}.docx

./Teofan Zavoratul:
Teofan Zavoratul - Arta rugaciunii 0.9 '{Religie}.docx
Teofan Zavoratul - Mantuirea in viata de familie 0.99 '{Religie}.docx
Teofan Zavoratul - Patericul lavrei sfantului Sava 0.9 '{Religie}.docx
Teofan Zavoratul - Pregatirea pentru spovedanie 0.9 '{Religie}.docx

./Teohar Mihadas:
Teohar Mihadas - Frumoasa risipa 1.0 '{Literatura}.docx
Teohar Mihadas - Inaltele acele vremi 1.0 '{Literatura}.docx
Teohar Mihadas - Pe muntele Ebal 1.0 '{Literatura}.docx
Teohar Mihadas - Pinii de pe Golna 1.0 '{Literatura}.docx
Teohar Mihadas - Steaua cainelui 1.0 '{Literatura}.docx
Teohar Mihadas - Taramul izvoarelor 1.0 '{Literatura}.docx

./Terentiu:
Terentiu - Cel ce se pedepseste singur 1.0 '{Teatru}.docx
Terentiu - Eunucul 1.0 '{Teatru}.docx
Terentiu - Fratii 0.7 '{Teatru}.docx

./Teresa Dawson:
Teresa Dawson - Barbatul renegat 0.9 '{Dragoste}.docx
Teresa Dawson - Festivalului vinului 0.99 '{Dragoste}.docx
Teresa Dawson - O singura pasiune 0.9 '{Dragoste}.docx

./Teresa Francis:
Teresa Francis - Amant pagan 0.99 '{Dragoste}.docx
Teresa Francis - La bine si la rau 0.99 '{Dragoste}.docx

./Teresa Medeiros:
Teresa Medeiros - A ta pana in zori 1.0 '{Romance}.docx
Teresa Medeiros - Blestemul frumusetii 1.0 '{Romance}.docx
Teresa Medeiros - Hotul de inimi 1.0 '{Romance}.docx
Teresa Medeiros - In cautarea dragostei 1.0 '{Romance}.docx
Teresa Medeiros - Lectii de seductie 1.0 '{Romance}.docx
Teresa Medeiros - Mireasa rapita 0.9 '{Romance}.docx
Teresa Medeiros - Mireasa si bestia 1.0 '{Romance}.docx
Teresa Medeiros - O noapte compromitatoare 1.0 '{Romance}.docx
Teresa Medeiros - Savoarea unui sarut 1.0 '{Romance}.docx
Teresa Medeiros - Un sarut de neuitat 1.0 '{Romance}.docx

./Teresa Solana:
Teresa Solana - O crima imperfecta 1.0 '{Politista}.docx

./Terry Bisson:
Terry Bisson - In odaia din varf 0.99 '{SF}.docx
Terry Bisson - Macsii 0.99 '{SF}.docx
Terry Bisson - Sunt facuti din carne 1.0 '{SF}.docx

./Terry Brooks:
Terry Brooks - Primul rege Shannara 1.0 '{SF}.docx

./Terry Hayes:
Terry Hayes - Calatorul 1.0 '{Thriller}.docx

./Terry Lawrence:
Terry Lawrence - Casa de vacanta 0.9 '{Dragoste}.docx
Terry Lawrence - Crede intr-o dragoste de durata 0.99 '{Dragoste}.docx

./Terry Prachett:
Terry Prachett - Trilogia Nomilor - V1 Camionul 0.99 '{AventuraTineret}.docx
Terry Prachett - Trilogia Nomilor - V2 Excavatorul 0.99 '{AventuraTineret}.docx
Terry Prachett - Trilogia Nomilor - V3 Nava 0.99 '{AventuraTineret}.docx

./Terry Pratchett:
Terry Pratchett - Lumea Disc - V01 Culoarea magiei 1.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V02 Lumina fantastica 2.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V03 Magie de ambe sexe 2.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V04 Mort 1.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V05 Copilul minune 2.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V06 Stranii surate 2.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V07 Piramide 2.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V08 Garzi! Garzi! 2.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V09 Eric 2.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V10 Imagini miscatoare 4.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V11 Domnul cu coasa 1.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V12 Prin cele strainatati 1.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V13 Zei marunti 1.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V14 Seniori si doamne 1.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V28 Uluitorul Maurice si rozatoarele lui educate 1.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V30 Scotidusii liberi V2.0 '{AventuraTineret}.docx
Terry Pratchett - Lumea Disc - V32 O palarie plina de cer 1.0 '{AventuraTineret}.docx
Terry Pratchett - Pamantul lung 1.0 '{AventuraTineret}.docx
Terry Pratchett - Semne bune 1.0 '{AventuraTineret}.docx

./Teru Miyamoto:
Teru Miyamoto - Brocart de toamna 0.99 '{Literatura}.docx

./Tessa Dare:
Tessa Dare - Stud Club - V1 Un dans cu un duce 1.0 '{Romance}.docx
Tessa Dare - Stud Club - V2 Dubla tentatie 1.0 '{Romance}.docx
Tessa Dare - Stud Club - V3 Trei nopti de dorinte 1.0 '{Romance}.docx

./Tess Gerritsen:
Tess Gerritsen - Rizzoli & Isles - V1 Chirurgul 1.0 '{Politista}.docx
Tess Gerritsen - Rizzoli & Isles - V2 Ucenicul 1.0 '{Politista}.docx
Tess Gerritsen - Rizzoli & Isles - V3 Pacatoasa 1.0 '{Politista}.docx
Tess Gerritsen - Rizzoli & Isles - V4 Dublura 1.0 '{Politista}.docx
Tess Gerritsen - Rizzoli & Isles - V5 Disparitia 1.0 '{Politista}.docx
Tess Gerritsen - Rizzoli & Isles - V6 Clubul Mefisto 1.0 '{Politista}.docx

./Theodor Constantin:
Theodor Constantin - Adevarul despre Luca Cristogel 1.0 '{Politista}.docx
Theodor Constantin - Balthazar soseste luni 1.0 '{Politista}.docx
Theodor Constantin - Casa de pe colina 1.0 '{Politista}.docx
Theodor Constantin - Cel mai lung amurg 1.0 '{Politista}.docx
Theodor Constantin - Cum a murit Claudiu Azimioara 1.0 '{Politista}.docx
Theodor Constantin - Enigma 'Profesor Rebegea' 1.0 '{Politista}.docx
Theodor Constantin - Johnny Boamba 2.0 '{Politista}.docx
Theodor Constantin - La miezul noptii va cadea o stea 2.0 '{Politista}.docx
Theodor Constantin - Nebel - V1 Fiul lui Monte Cristo V1 2.0 '{Politista}.docx
Theodor Constantin - Nebel - V1 Fiul lui Monte Cristo V2 2.0 '{Politista}.docx
Theodor Constantin - Nebel - V2 Capitanul de cursa lunga 1.0 '{Politista}.docx
Theodor Constantin - Nebel - V3 Doamna in mov 1.0 '{Politista}.docx
Theodor Constantin - Nebel - V5 Asta seara, relache 1.0 '{Politista}.docx
Theodor Constantin - Nebel - V6 Doamna cu voaleta din Balt Orient Expres 1.0 '{Politista}.docx
Theodor Constantin - Nebel - V7 Matraguna contra monseniorului V1 1.0 '{Politista}.docx
Theodor Constantin - Nebel - V7 Matraguna contra monseniorului V2 1.0 '{Politista}.docx
Theodor Constantin - Nebel - V8 Muntele mortii 1.0 '{Politista}.docx
Theodor Constantin - Nebel - V9 Magdalena de la miezul noptii 1.0 '{Politista}.docx
Theodor Constantin - Nebel - V10 Crizanteme pentru Erna 1.0 '{Politista}.docx
Theodor Constantin - Razbunarea lui Ionut 1.0 '{Politista}.docx
Theodor Constantin - Se destrama noaptea 1.0 '{Politista}.docx
Theodor Constantin - Urmarirea abia incepe 1.0 '{Politista}.docx

./Theodore Sturgeon:
Theodore Sturgeon - Dumnezeu pentru un microcosmos 1.0 '{SF}.docx
Theodore Sturgeon - Mai mult decat oameni 1.0 '{SF}.docx
Theodore Sturgeon - Nestemate visatoare 1.1 '{SF}.docx
Theodore Sturgeon - Sculptura lenta 0.99 '{SF}.docx

./Theodor Fontane:
Theodor Fontane - Pacatul. Cecile 1.0 '{Dragoste}.docx

./Theodor Mommsen:
Theodor Mommsen - Istoria Romana V1-4 0.99 '{Istorie}.docx

./Theodor Storm:
Theodor Storm - Micutul Hawelmann 1.0 '{BasmesiPovesti}.docx

./Theo Montera:
Theo Montera - Tratat practic de astrologie medicala 0.9 '{Spiritualitate}.docx

./Theophile Gautier:
Theophile Gautier - Domnisoara Maupin 1.0 '{ClasicSt}.docx

./Theresa Dale:
Theresa Dale - Drumul discordiei 0.99 '{Dragoste}.docx
Theresa Dale - Madona cu parul de aur 0.9 '{Romance}.docx

./Thierry Bizot:
Thierry Bizot - Paradisul derizoriu 1.0 '{Diverse}.docx

./Thomas Cathcart & Daniel Klein:
Thomas Cathcart & Daniel Klein - Aristotel si furnicarul merg la Washington 1.0 '{Umor}.docx
Thomas Cathcart & Daniel Klein - Platon si ornitorincul intra intr-un bar 1.0 '{Umor}.docx

./Thomas Enger:
Thomas Enger - Henning Juul - V1 Ars 1.0 '{Diverse}.docx

./Thomas Gifford:
Thomas Gifford - Assassini 1.0 '{Thriller}.docx
Thomas Gifford - Operatiunea Praetorian 1.1 '{ActiuneComando}.docx

./Thomas Hardy:
Thomas Hardy - Departe de lumea dezlantuita 1.0 '{ClasicSt}.docx
Thomas Hardy - Doi ochi albastri 1.0 '{Dragoste}.docx
Thomas Hardy - Idila pe un turn 1.0 '{Dragoste}.docx
Thomas Hardy - Jude nestiutul V1 1.0 '{ClasicSt}.docx
Thomas Hardy - Jude nestiutul V2 1.0 '{ClasicSt}.docx
Thomas Hardy - Micile ironii ale vietii 0.6 '{ClasicSt}.docx
Thomas Hardy - Primarul din Casterbridge 1.0 '{ClasicSt}.docx
Thomas Hardy - Tess d'Urberville V1 1.0 '{ClasicSt}.docx
Thomas Hardy - Tess d'Urberville V2 1.0 '{ClasicSt}.docx

./Thomas Harris:
Thomas Harris - Cari Mora 1.0 '{Literatura}.docx
Thomas Harris - Hannibal Lecter - V1 Dragonul rosu 1.0 '{Thriller}.docx
Thomas Harris - Hannibal Lecter - V2 Tacerea mieilor 1.9 '{Thriller}.docx
Thomas Harris - Hannibal Lecter - V3 Doctor Hannibal 1.9 '{Thriller}.docx
Thomas Harris - Hannibal Lecter - V4 Hannibal in spatele mastii 1.0 '{Thriller}.docx

./Thomas Josiah Dimsdale:
Thomas Josiah Dimsdale - Banda serifului Plummer 1.0 '{Western}.docx

./Thomas Keneally:
Thomas Keneally - Lista lui Schindler 3.0 '{Thriller}.docx

./Thomas Lee Jacobs:
Thomas Lee Jacobs - Tehnica vindecarii cu energie universala 0.2 '{Spiritualitate}.docx

./Thomas Malory:
Thomas Malory - Tristan si Iseut 1.0 '{Dragoste}.docx

./Thomas Mann:
Thomas Mann - Alesul 0.9 '{ClasicSt}.docx
Thomas Mann - Alteta regala 1.0 '{ClasicSt}.docx
Thomas Mann - Casa Buddenbrook 2.0 '{ClasicSt}.docx
Thomas Mann - Doctor Faustus 0.99 '{ClasicSt}.docx
Thomas Mann - Iosif si fratii sai V1 1.0 '{ClasicSt}.docx
Thomas Mann - Iosif si fratii sai V2 1.0 '{ClasicSt}.docx
Thomas Mann - Iosif si fratii sai V3 1.0 '{ClasicSt}.docx
Thomas Mann - Moartea la Venetia 1.0 '{ClasicSt}.docx
Thomas Mann - Muntele vrajit 2.0 '{ClasicSt}.docx
Thomas Mann - Nuvele 1.0 '{ClasicSt}.docx
Thomas Mann - Tristan 1.0 '{ClasicSt}.docx

./Thomas Nashe:
Thomas Nashe - Peripetiile napastuitului calator 0.6 '{Literatura}.docx

./Thomas Pinchon:
Thomas Pinchon - Strigarea lotului 49 0.9 '{Diverse}.docx

./Tiberiu Stan:
Tiberiu Stan - Sinuciderea 0.99 '{ProzaScurta}.docx

./Tiberiu Vornic:
Tiberiu Vornic - Intamplari din pragul veacului 0.9 '{IstoricaRo}.docx

./Tibor Ostermann:
Tibor Ostermann - Vesnic prizonier 1.0 '{Razboi}.docx

./Tibor Rode:
Tibor Rode - Virusul Mona Lisa 1.0 '{Politista}.docx

./Tim Johnston:
Tim Johnston - Coborarea 1.0 '{Diverse}.docx

./Tim Lahaye:
Tim Lahaye - Supravietuitorii - V1 Supravietuitorii 1.0 '{SF}.docx
Tim Lahaye - Supravietuitorii - V2 Armata patimirilor 1.0 '{SF}.docx
Tim Lahaye - Supravietuitorii - V3 Nicolae 1.0 '{SF}.docx
Tim Lahaye - Supravietuitorii - V4 Pecetluirea sfintilor 1.0 '{SF}.docx
Tim Lahaye - Supravietuitorii - V5 Apollyon 1.0 '{SF}.docx

./Timmy Bradley:
Timmy Bradley - Crima de la manastirea Saint Bernard 1.0 '{Politista}.docx

./Timons Esaias:
Timons Esaias - Judecata de apoi 0.99 '{SF}.docx
Timons Esaias - Locul prabusirii 0.99 '{SF}.docx
Timons Esaias - Norbert si sistemul 0.99 '{SF}.docx

./Timothy Harris:
Timothy Harris - American gigolo 1.0 '{Thriller}.docx

./Timothy Zahn:
Timothy Zahn - Odiseea Dragonului - V1 Hot si dragon 1.0 '{Aventura}.docx
Timothy Zahn - Odiseea Dragonului - V2 Soldat si dragon 1.0 '{Aventura}.docx

./Tim Powers:
Tim Powers - Regele pescar 1.0 '{SF}.docx

./Timur Vermes:
Timur Vermes - Ia uite cine s-a intors 1.0 '{Literatura}.docx

./Tina Seskis:
Tina Seskis - Cu un pas prea departe 1.0 '{Literatura}.docx

./Tirso de Molina:
Tirso de Molina - Seducatorul din Sevilla 0.99 '{Teatru}.docx

./Titu Dinut:
Titu Dinut - Comandirul 0.7 '{Diverse}.docx

./Titu Maiorescu:
Titu Maiorescu - Comediile d-lui I.L. Caragiale 0.9 '{Critica}.docx
Titu Maiorescu - Cugetari si aforisme 1.0 '{Filozofie}.docx
Titu Maiorescu - Eminescu si poeziile lui 1.0 '{Critica}.docx
Titu Maiorescu - O cercetare critica asupra poeziei romane 1.0 '{Critica}.docx

./Titu Popescu:
Titu Popescu - De la paradox la paradoxism 0.9 '{Diverse}.docx

./Titus Maccius Plautus:
Titus Maccius Plautus - Cei doi gemeni 0.9 '{Teatru}.docx
Titus Maccius Plautus - Militarul fanfaron 0.9 '{Teatru}.docx
Titus Maccius Plautus - Ulcica 0.9 '{Teatru}.docx

./Titus Popovici:
Titus Popovici - Mostenirea 1.0 '{IstoricaRo}.docx

./Tobias:
Tobias - Seriile ascensiunii 0.8 '{Spiritualitate}.docx

./Toby Clements:
Toby Clements - Codul Da Vino 0.99 '{Thriller}.docx

./Toby Litt:
Toby Litt - Cadaverosimil 0.9 '{Politista}.docx

./Toea Muresan:
Toea Muresan - 8 martie 1.0 '{Teatru}.docx

./Tomas Borec:
Tomas Borec - Buna ziua domnule Ampere 1.0 '{Biografie}.docx

./Tomas Martinez:
Tomas Martinez - Zborul reginei 0.99 '{Literatura}.docx

./Tom Cain:
Tom Cain - Tinta 1.0 '{Thriller}.docx

./Tom Clancy:
Tom Clancy - Centrul de comanda - V01 Centrul de comanda 1.0 '{ActiuneComando}.docx
Tom Clancy - Centrul de comanda - V02 Imaginea din oglinda 1.0 '{ActiuneComando}.docx
Tom Clancy - Centrul de comanda - V03 Jocuri de stat 1.0 '{ActiuneComando}.docx
Tom Clancy - Centrul de comanda - V04 Acte de razboi 1.0 '{ActiuneComando}.docx
Tom Clancy - Centrul de comanda - V05 Echilibru de forte 1.0 '{ActiuneComando}.docx
Tom Clancy - Centrul de comanda - V06 Stare de asediu 1.0 '{ActiuneComando}.docx
Tom Clancy - Centrul de comanda - V07 Dezbina si cucereste 1.0 '{ActiuneComando}.docx
Tom Clancy - Furtuna rosie 2.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V01 Fara sovaire 1.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V02 Jocuri de putere 1.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V03 Vanatoarea lui Octombrie Rosu 2.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V04 Cardinalul de la Kremlin 1.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V05 Pericol iminent 2.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V06 Duel la inaltime V1 2.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V07 Duel la inaltime V2 2.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V08 Datorie de onoare V1 2.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V09 Datorie de onoare V2 2.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V10 Ordine prezidentiale V1 3.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V11 Ordine prezidentiale V2 3.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V12 Rainbow Six V1 2.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V13 Rainbow Six V2 1.0 '{ActiuneComando}.docx
Tom Clancy - J. Ryan & J. Clark - V14 Ursul si dragonul 2.0 '{ActiuneComando}.docx
Tom Clancy - Tinta marcata 1.0 '{ActiuneComando}.docx
Tom Clancy - Viu sau mort 1.0 '{ActiuneComando}.docx

./Tom Eaton:
Tom Eaton - Codul cavalerilor 2.0 '{AventuraIstorica}.docx

./Tom Egeland:
Tom Egeland - Bjorn Belto - V1 Capatul cercului 1.0 '{AventuraIstorica}.docx
Tom Egeland - Bjorn Belto - V2 Paznicii legamantului 1.0 '{AventuraIstorica}.docx

./Tom Gabbay:
Tom Gabbay - Conspiratia din Berlin 3.0 '{Politista}.docx

./Tom Holt:
Tom Holt - Asteptau pe cineva mai inalt 1.0 '{Tineret}.docx

./Tomi Adeyemi:
Tomi Adeyemi - Zestrea Orishei - V1 Urmasii de sange si os 1.0 '{Supranatural}.docx

./Tom Knox:
Tom Knox - Secretul Genezei 1.0 '{AventuraIstorica}.docx

./Tom Martin:
Tom Martin - Piramida 1.0 '{AventuraIstorica}.docx

./Tommy Wallach:
Tommy Wallach - Si-am privit totii spre cer 1.0 '{Tineret}.docx

./Tom Rachman:
Tom Rachman - Imperfectionistii 0.99 '{Thriller}.docx

./Tom Reamy:
Tom Reamy - Tanarul Detweiler 2.0 '{SF}.docx

./Tom Rob Smith:
Tom Rob Smith - Agentul 6 1.0 '{Thriller}.docx
Tom Rob Smith - Copilul 44 1.0 '{Thriller}.docx
Tom Rob Smith - Ferma 1.0 '{Thriller}.docx
Tom Rob Smith - Raportul secret 1.0 '{Thriller}.docx

./Tom Sharpe:
Tom Sharpe - Marea aspiratie 0.99 '{Thriller}.docx
Tom Sharpe - V1 Wilt 0.9 '{Diverse}.docx
Tom Sharpe - V2 Alternativa Wilt 0.9 '{Diverse}.docx

./Toni Anderson:
Toni Anderson - Patima intunecata 1.0 '{Erotic}.docx

./Tony Chester:
Tony Chester - Ultima lupta, prima lupta 0.8 '{ActiuneComando}.docx

./Tony Hillerman:
Tony Hillerman - Lupul din umbra 1.0 '{Thriller}.docx

./Tony Parsons:
Tony Parsons - Copil si barbat 1.0 '{Literatura}.docx
Tony Parsons - Trusa criminalistica 1.0 '{Thriller}.docx

./Tor Age Bringsvaerd:
Tor Age Bringsvaerd - Codemus 0.99 '{Diverse}.docx

./Toru Namikoshi:
Toru Namikoshi - Shiatsu & Stretching 0.99 '{Sanatate}.docx

./Tracey Garvis Graves:
Tracey Garvis Graves - Pe insula 1.0 '{Literatura}.docx

./Traci Chee:
Traci Chee - Cititoarea 1.0 '{Supranatural}.docx

./Traci Harding:
Traci Harding - Trilogia Mistica - V1 Gena lui Isis 1.0 '{SF}.docx
Traci Harding - Trilogia Mistica - V2 Reginele dragon 1.0 '{SF}.docx

./Tracy Brogan:
Tracy Brogan - Bell Harbor - V1 Nebunie de-o vara 1.0 '{Literatura}.docx

./Tracy Cavalier:
Tracy Cavalier - Fata cu cercel de perla 1.0 '{Literatura}.docx
Tracy Cavalier - Ingeri cazatori 0.99 '{Literatura}.docx

./Tracy Pearson:
Tracy Pearson - Casnicie in pericol 0.99 '{Dragoste}.docx
Tracy Pearson - Inelul de logodna 0.9 '{Romance}.docx
Tracy Pearson - Tu esti sau sora ta 0.9 '{Dragoste}.docx

./Traian Tandin:
Traian Tandin - Adio Ringo 2.0 '{Politista}.docx
Traian Tandin - Dilemele capitanului Roman 2.0 '{Politista}.docx
Traian Tandin - Din dosarele comisarului Tandin 1.0 '{Politista}.docx
Traian Tandin - Grind, urmasul lui Ringo 1.0 '{Politista}.docx
Traian Tandin - Secte criminale 0.99 '{Politista}.docx

./Traian Uba:
Traian Uba - Caseta 1.0 '{ClubulTemerarilor}.docx

./Trail Armitage:
Trail Armitage - Scarface, regele gangsterilor 0.9 '{Politista}.docx

./Trevor Norton:
Trevor Norton - Ochi holbati si par valvoi 0.9 '{MistersiStiinta}.docx

./Trish Cook:
Trish Cook - Sub soarele noptii 1.0 '{Romance}.docx

./Trish Graves:
Trish Graves - Cel mai frumos cadou 0.99 '{Dragoste}.docx

./Truman Capote:
Truman Capote - Mic dejun la Tiffany 1.0 '{Literatura}.docx
Truman Capote - Miriam 1.0 '{Literatura}.docx

./Tucker Malarkey:
Tucker Malarkey - Reinvierea 1.0 '{AventuraIstorica}.docx

./Tudor Arghezi:
Tudor Arghezi - Cu bastonul prin Bucuresti 1.0 '{ClasicRo}.docx
Tudor Arghezi - Piatra pitigoiului 0.99 '{ClasicRo}.docx

./Tudor Calin Zarojanu:
Tudor Calin Zarojanu - Viata lui Corneliu Coposu 0.99 '{Biografie}.docx

./Tudor Chirila:
Tudor Chirila - Exercitii de echilibru 1.0 '{Diverse}.docx

./Tudor Greceanu:
Tudor Greceanu - Drumul celor putini 0.9 '{Razboi}.docx

./Tudor I. Oprea:
Tudor I. Oprea - Cum sa te prezinti cat mai bine la un interviu pentru o slujba 1.0 '{DezvoltarePersonala}.docx

./Tudor Musatescu:
Tudor Musatescu - Ca-n filme 1.0 '{Teatru}.docx
Tudor Musatescu - Geamandura 0.9 '{Teatru}.docx
Tudor Musatescu - Sosesc deseara 1.0 '{Teatru}.docx
Tudor Musatescu - Teatru scurt 1.0 '{Teatru}.docx
Tudor Musatescu - Visul unei nopti de iarna 1.0 '{Teatru}.docx

./Tudor Negoita:
Tudor Negoita - Cand zeii conspira 1.0 '{SF}.docx
Tudor Negoita - Cutia Pandorei 1.0 '{SF}.docx
Tudor Negoita - Sub semnul blandei terori 1.0 '{IstoricaRo}.docx
Tudor Negoita - Verificati ipoteza Nessus 1.0 '{SF}.docx

./Tudor Octavian:
Tudor Octavian - Sortiti iubirii 1.0 '{Dragoste}.docx

./Tudor Popescu:
Tudor Popescu - 4 comedii 0.99 '{Teatru}.docx
Tudor Popescu - Cel mai istet, cel mai viteaz 1.0 '{IstoricaRo}.docx
Tudor Popescu - Nu ne nastem toti la aceiasi varsta 1.0 '{Teatru}.docx
Tudor Popescu - Scaunul 1.0 '{Teatru}.docx
Tudor Popescu - Un dac la Roma 1.0 '{Literatura}.docx

./Tudor Teodorescu Braniste:
Tudor Teodorescu Braniste - Pavilionul de vanatoare 1.0 '{Literatura}.docx
Tudor Teodorescu Braniste - Printul 1.0 '{Literatura}.docx
Tudor Teodorescu Braniste - Scandal 1.0 '{Literatura}.docx

./Tui T. Sutherland:
Tui T. Sutherland - Avatar - V1 Deci asa se sfarseste totul 1.0 '{AventuraTineret}.docx

./Tyson Mauermann:
Tyson Mauermann - Moarte la 900 m 1.0 '{SF}.docx
```

